4번 보기

create table 시스템사용이력 (
    메뉴id varchar(10),
    사용유형코드 varchar(10)
)\\

insert into 시스템사용이력 values ('1', 's1')\\
insert into 시스템사용이력 values ('2', 's2')\\
insert into 시스템사용이력 values ('3', 's3')\\
insert into 시스템사용이력 values ('4', 's4')\\
insert into 시스템사용이력 values ('5', 's5')\\
insert into 시스템사용이력 values ('6', 's6')\\
insert into 시스템사용이력 values ('1', 'b1')\\
insert into 시스템사용이력 values ('2', 'b2')\\
insert into 시스템사용이력 values ('3', 'b3')\\
insert into 시스템사용이력 values ('4', 'b3')\\
insert into 시스템사용이력 values ('5', 'b4')\\
insert into 시스템사용이력 values ('6', 'b4')\\


SELECT 메뉴ID, 사용유형코드, 
              AVG(COUNT(*)) AS AVGCNT
FROM 시스템사용이력
GROUP BY 메뉴ID, 사용유형코드\\
==> ORA-00937: not a single-group group function

SELECT AVG(COUNT(*)) AS AVGCNT
FROM 시스템사용이력
GROUP BY 메뉴ID, 사용유형코드
==> 1
==> 중첩된 그룹함수(AVG(COUNT(*)), 그룹함수(그룹함수(컬럼))은 결과값이 1건이 될 수밖에 없음
    SELECT절에 메뉴ID, 사용유형코드를 출력 하지 못하므로 SELECT절을 그대로 사용 시 ERROR
