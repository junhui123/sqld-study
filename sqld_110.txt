create table 자재발주 (
 자재번호 number,
 발주처ID varchar(3),
 발주일자 date,
 발주수량 number
);

insert into 자재발주 values(1, '001', to_date('20150102', 'yyyymmdd'), 100);
insert into 자재발주 values(1, '001', to_date('20150103', 'yyyymmdd'), 200);
insert into 자재발주 values(2, '001', to_date('20150102', 'yyyymmdd'), 200);
insert into 자재발주 values(2, '002', to_date('20150102', 'yyyymmdd'), 100);
insert into 자재발주 values(3, '001', to_date('20150103', 'yyyymmdd'), 100);
insert into 자재발주 values(3, '002', to_date('20150103', 'yyyymmdd'), 200);

select case when grouping(자재번호)=1 then '자재전체' else 자재번호 end as 자재번호,
case when grouping(발주처ID)=1 then '발주처전체' else 발주처ID end as 발주처ID,
case when grouping(발주일자)=1 then '발주일자전체' else 발주일자 end as 발주일자,
sum(발주수량) as 발주수량합계
from 자재발주
group by 자재번호
order by 자재번호, 발주처ID, 발주일자


-----------------------------------------------------------------------------
select case when grouping(자재번호)=1 then '자재전체' else 자재번호 end as 자재번호,
case when grouping(발주처ID)=1 then '발주처전체' else 발주처ID end as 발주처ID,
case when grouping(발주일자)=1 then '발주일자전체' else 발주일자 end as 발주일자,
sum(발주수량) as 발주수량합계
from 자재발주
order by 자재번호, 발주처ID, 발주일자
=>grouping은 group by를 필요로함
-----------------------------------------------------------------------------