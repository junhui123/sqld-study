CREATE TABLE TAB1 (
 COL1 VARCHAR(1),
 COL2 VARCHAR(1)
);

INSERT INTO TAB1 VALUES ('a', NULL);
INSERT INTO TAB1 VALUES ('b', '');
INSERT INTO TAB1 VALUES ('c', 3);
INSERT INTO TAB1 VALUES ('d', 4);
INSERT INTO TAB1 VALUES ('e', 5);

select col2 from tab1 where col1='b' 
=> 1행 null 출력

select nvl(col2, 'x') from tab1 where col1='a'    : oracle
select isnull(col2, 'x') from tab1 col1='a'       : sql server
=> 1행 x출력

select count(col1) from tab1 where col2 = null
=> 1행 0출력

select count(col2) from tab1 where col1 in ('b', 'c')
=> 1행 1출력