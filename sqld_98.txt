create table 회원 (
 회원ID varchar(20) primary key,
 회원명 varchar(20),
 가입일시 date,
 이메일 varchar(30) 
);

create table 이벤트(
 이벤트ID varchar(20) primary key,
 이벤트명 varchar(20),
 시작일자 date,
 종료일자 date,
 내용 varchar(100)
);

create table 메일발송(
 이벤트ID varchar(20) constraint FK_이벤트ID references 이벤트,
 회원ID varchar(20) constraint FK_회원ID references 회원,
 발송일시 date
);

insert into 회원 values ('test1', 'test1', to_date('2013.03.21', 'yyyy.mm.dd'), 'test1@test');
insert into 회원 values ('test2', 'test2', to_date('2014.04.22', 'yyyy.mm.dd'), 'test2@test');
insert into 회원 values ('test3', 'test3', to_date('2015.05.23', 'yyyy.mm.dd'), 'test3@test');
insert into 회원 values ('test4', 'test4', to_date('2016.05.01', 'yyyy.mm.dd'), 'test4@test');
insert into 회원 values ('test5', 'test5', to_date('2017.01.18', 'yyyy.mm.dd'), 'test5@test');

insert into 이벤트 values ('event1', 'event1 name', to_date('2014.03.20', 'yyyy.mm.dd'), to_date('2014.05.22', 'yyyy.mm.dd'), 'event1 event');

insert into 메일발송 values ('event1', 'test1', to_date('2014.03.21', 'yyyy.mm.dd'));
insert into 메일발송 values ('event1', 'test2', to_date('2014.03.21', 'yyyy.mm.dd'));


select A.회원ID, A.회원명, A.이메일
from 회원 A
where exists (
 select 'x'
 from 이벤트 B, 메일발송 C 
 where B.시작일자 >= to_date('2014.10.01', 'yyyy.mm.dd')
 and B.이벤트ID = C.이벤트ID
 and A.회원ID = C.회원ID
 having count(*) < (select count(*) from 이벤트 where 시작일자 >= to_date('2014.10.01', 'yyyy.mm.dd'))); 
 
 select A.회원ID, A.회원명, A.이메일, B.이벤트ID, B.이벤트명, B.시작일자, B.종료일자, C.발송일시
 from 회원 A, 이벤트 B, 메일발송 C
 where B.시작일자 >= to_date('2014.10.01', 'yyyy.mm.dd')
 and B.이벤트ID = C.이벤트ID
 and A.회원ID = C.회원ID;
 
 select 'x'
 from 회원 A, 이벤트 B, 메일발송 C
 where B.시작일자 >= to_date('2014.10.01', 'yyyy.mm.dd')
 and B.이벤트ID = C.이벤트ID
 and A.회원ID = C.회원ID; 
  
 select count(*) from 이벤트 where 시작일자 >= to_date('2014.10.01', 'yyyy.mm.dd');
  
 select count(*)
 from 회원 A, 이벤트 B, 메일발송 C 
 where B.시작일자 >= to_date('2014.10.01', 'yyyy.mm.dd')
 and B.이벤트ID = C.이벤트ID
 and A.회원ID = C.회원ID;
  
 select 'x'
 from 회원 A, 이벤트 B, 메일발송 C 
 where B.시작일자 >= to_date('2014.10.01', 'yyyy.mm.dd')
 and B.이벤트ID = C.이벤트ID
 and A.회원ID = C.회원ID
 having count(*) < (select count(*) from 이벤트 where 시작일자 >= to_date('2014.10.01', 'yyyy.mm.dd')); 
--------------------------------------------------------------------------------------------------------------------------

insert into 회원 values ('test1', 'test1', to_date('2013.03.21', 'yyyy.mm.dd'), 'test1@test');
insert into 회원 values ('test2', 'test2', to_date('2014.04.22', 'yyyy.mm.dd'), 'test2@test');
insert into 회원 values ('test3', 'test3', to_date('2015.05.23', 'yyyy.mm.dd'), 'test3@test');
insert into 회원 values ('test4', 'test4', to_date('2016.05.01', 'yyyy.mm.dd'), 'test4@test');
insert into 회원 values ('test5', 'test5', to_date('2017.01.18', 'yyyy.mm.dd'), 'test5@test');

insert into 이벤트 values ('event1', 'event1 name', to_date('2014.01.20', 'yyyy.mm.dd'), to_date('2017.03.22', 'yyyy.mm.dd'), 'event1 event');
insert into 이벤트 values ('event2', 'event2 name', to_date('2016.03.10', 'yyyy.mm.dd'), to_date('2018.04.01', 'yyyy.mm.dd'), 'event2 event');
insert into 이벤트 values ('event3', 'event3 name', to_date('2017.04.20', 'yyyy.mm.dd'), to_date('2018.05.24', 'yyyy.mm.dd'), 'event3 event');

insert into 메일발송 values ('event1', 'test1', to_date('2014.01.21', 'yyyy.mm.dd'));
insert into 메일발송 values ('event2', 'test1', to_date('2016.03.11', 'yyyy.mm.dd'));
insert into 메일발송 values ('event3', 'test1', to_date('2017.04.21', 'yyyy.mm.dd'));
insert into 메일발송 values ('event2', 'test3', to_date('2016.03.11', 'yyyy.mm.dd'));
insert into 메일발송 values ('event3', 'test5', to_date('2017.04.21', 'yyyy.mm.dd'));

--------------------------------------------------------------------------------------------------------------------------
